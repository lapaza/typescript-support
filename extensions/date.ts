interface Date {

    toDaysStart: () => Date;

    toDaysEnd: () => Date;

    isTheSameDay: (other: Date) => boolean;

}

Date.prototype.toDaysStart = function(): Date {
    const self = this;
    self.setHours(0);
    self.setMinutes(0);
    self.setSeconds(0);
    self.setMilliseconds(0);
    return self;
}

Date.prototype.toDaysEnd = function(): Date {
    const self = this;
    self.setHours(23);
    self.setMinutes(59);
    self.setSeconds(59);
    self.setMilliseconds(999);
    return self;
}

Date.prototype.isTheSameDay = function(other: Date): boolean {
    const self = this;
    return self.getDate() === other.getDate()
        && self.getMonth() === other.getMonth()
        && self.getFullYear() === other.getFullYear();
}
